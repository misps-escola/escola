<?php

	#
	require 'sso/functions.php';
	require 'src/escola.php';

	#
	$escola = new Escola();
	$sso = new SSO_Client();

	#
	$sso->is_loggedin();

?>
<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Escola de atendimento - Agentes</title>
		<link rel="stylesheet" type="text/css" href="./assets/css/escola-dev.css" />
		<script type="text/javascript" src="./assets/js/jquery-2.0.1.min.js"></script>
		<script type="text/javascript" src="./assets/js/jquery.dataTables.js"></script>
		<script type="text/javascript" src="./assets/js/escola.js"></script>
		<script type="text/javascript" src="https://www.google.com/jsapi"></script>
		<script type="text/javascript">
			google.load("visualization", "1", {packages:["corechart"]});
		</script>
	</head>
	<body>
		<div class="user-data">
			<div class="inner">
				<ul>
					<li class="fr logout"><a href="./sso/logout" class="btn-logout"><span class="fr descr-btn">Sair</span></a></li>
					<li class="fr user-meta"><span>Bem vindo(a), </span><span class="bold capitalized user-name"><?php echo $sso->get_user_data('full_name');?></span></li>
				</ul>
				<div class="clear"></div>
			</div>
		</div>

		<div class="banner">
			<div class="header">
				<div class="inner">
					<div class="fl header-logo">
						<a href="./" title="Ir para página inicial"><img src="./assets/img/logo.png" alt="" /></a>
					</div>
					<div class="fr header-menu">
						<ul>
							<li class="fr"><a href="./turmas">Resultados turma</a></li>
							<li class="fr"><a href="./agentes">Resultados agente</a></li>
						</ul>
					</div>
					<div class="fr header-breadcrumb" id="breadcrumb"></div>
					<div class="clear"></div>
				</div>
			</div>
			<div class="baseline"></div>
		</div>

		<div class="content">
			<div class="inner">
				<div id="filtro" name="filtro" class="filtros filtro-escola">
					<div id='turmas' class='fl filtro-esquerda'><label for='turmas'>Turma: </label><?php echo $escola->combo_turmas();?></div>
					<div id='agentes' class='fr filtro-direita'></div>
					<div class="clear"></div>
				</div>

				<div id="resultados" name="resultados" class="resultados resultados-agente" style="display:none;">
					<div id="score" class="grafico ">GRÁFICO SCORE</div>
					<div id="atendidas" class="grafico ">GRÁFICO ATENDIDAS</div>
					<div id="tmo" class="grafico ">GRÁFICO TMO</div>				
					<div id="qualidade" class="grafico ">GRÁFICO QUALIDADE</div>
					<div id="faltas" class="grafico ">GRÁFICO FALTAS</div>
				</div>

				<div id="json" style="display:none;"></div>




				<div class="clear"></div>
			</div><!-- /end inner -->		
		</div><!-- /end content -->

		<div class="footer">
			<div class="inner">
				<span id="copy">© <?php echo date('Y');?> Porto Seguro - Todos os direitos reservados.</span>
				<span class="fr"><a href="versao.mis">notas da versão 2.0.</a></span>
			</div>
		</div>

	
	</body>
</html>
