<?php

	

	class SSO_Client{

		private $base_path;
		private $session;
		private $session_expires;
		private $session_name;
		private $session_lifetime;

		function __construct(){
			$this->base_path        = dirname(__FILE__);
			$this->session_name     = $this->get_config('session','name');
			$this->session_lifetime = $this->get_config('session','lifetime');
			$this->session_savepath = $this->base_path.'\data\/';
			$this->session_expires  = time()+$this->session_lifetime;
		}

		function base_url(){
			return $this->get_config('service_url','continue');
		}

		# continue after login/logout
		function continue_after(){
			if(empty($_COOKIE['continue'])){
				return $this->get_config('service_url','continue');
			}
			else{
				return (preg_match('/^login/', $this->decode($_COOKIE['continue']))) ? $this->get_config('service_url','continue') : base64_decode($_COOKIE['continue']) ;
			}
		}

		function dir_exists($path){
			if(is_dir($path) == false){
				mkdir($path);
			}

			return true;
		}

		function do_login($hash){
			$this->session_write($hash);
			$this->session_write_data($hash);
			$this->write_cookie('splash', 1, time()+2592000); # modal de aviso sobre o SSO

			$this->redirect_to($this->continue_after());
		}

		function fatal_error($msg){
			header('Content-Type: text/html; charset=utf-8');
			echo '<h1>Erro Fatal</h1>';
			echo $msg;
			exit;
		}

		function file_exists($file){
			if(!is_file($file)){
				$this->fatal_error('Ocorreu um erro interno nesta aplicação. Contate um administrador do sistema caso o erro persista. <br><br>#001');
			}

			return true;
		}

		function get_config($type,$item){
			$setting = json_decode(file_get_contents($this->base_path.'\settings'), true);
			return (empty($setting[$type][$item])) ? false : $setting[$type][$item] ;
		}

		function get_current_url(){
			$preffix = (empty($_SERVER['HTTPS'])) ? 'http://' : 'https://' ;
			return $preffix.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		}

		function get_ip(){
			return (empty($_SERVER['HTTP_X_FORWARDED_FOR'])) ? $_SERVER['REMOTE_ADDR'] : $_SERVER['HTTP_X_FORWARDED_FOR'] ;
		}

		function get_server_hash($hash){
			return substr($hash, 96, 40);
		}

		function get_session_hash($hash){
			return substr($hash, 32, 64);
		}

		function get_user_data($item){
			$session_file = $this->session_savepath.$this->session_id();

			if(!is_file($session_file)){
				$this->write_cookie('continue', $this->get_current_url());
				$this->redirect_to($this->get_config('service_url','login').'?dsc');
			}
			
			$data = json_decode(file_get_contents($session_file), true);
			return (empty($data[$item])) ? 0 : $data[$item] ;
		}

		# funciona em partes, rever e validar
		function is_loggedin(){
			# Dont have cookie? Not loggedin, simple!
			if(empty($_COOKIE[$this->session_name])){
				$this->write_cookie('continue', $this->get_current_url());
				$this->write_cookie('logout', 's');

				$this->redirect_to($this->get_config('service_url','login'));
			}

			# Então tem o cookie, verifica o tempo de vida
			if($this->session_lifetime() < 300){
				$this->redirect_to($this->session_validate());
			}
			else{
				$this->session_renew();
			}
		}

		function is_reliable($token){
			if($this->get_config('security','server_id') != $token){
				$this->logger("application ´".$token."´ not trusted access this resource");
				$this->fatal_error('Esta aplicação não está autorizada à acessar este recurso. Contate um administrador do sistema caso o erro persista. <br><br>#001');
			}
		}

		function is_there($param){
			if(empty($param)){
				$this->logger('An param isnt transmited on url');
				$this->fatal_error('Você não forneceu um ou mais parâmetros necessários para acessar este recurso. Contate um administrador do sistema caso o erro persista.');
			}
		}

		function logger($msg){
			$path = $path = $this->base_path.'\logs';

			if(is_dir($path) == false){
				mkdir($path);
			}
			error_log(date('d-m-Y H:i:s').' | '.$this->get_ip().' | '.$msg."\r\n", 3, $path.'\error');
		}

		function redirect_to($url){
			header("Location: {$url}");
			exit;
		}

		function session_destroy(){
			$this->write_cookie('logout', 'u');
			$this->destroy_cookie($this->session_name);
			$this->destroy_cookie('tm');

			$this->redirect_to($this->get_config('service_url','login'));
		}

		function session_id(){
			return $this->decode($_COOKIE[$this->session_name]);
		}

		function session_lifetime(){
			return (empty($_COOKIE['tm'])) ? 0 : $this->decode($_COOKIE['tm'])-time() ;
		}
		

		function session_renew(){
			if(empty($_COOKIE[$this->session_name]) && empty($_COOKIE['tm'])){
				$this->redirect_to($this->get_config('service_url','login'));
			}

			$this->write_cookie('tm', $this->session_expires, $this->session_expires);
			$this->write_cookie($this->session_name, $this->session_id(), $this->session_expires);
		}

		function session_validate(){
			$string = '?buttowski='.md5(uniqid().time()).$this->session_id().$this->get_config('security','service_id');

			$this->redirect_to($this->get_config('server_url','renew').$string);
		}

		function session_write($hash){
			$this->write_cookie('tm', $this->session_expires, $this->session_expires);
			$this->write_cookie($this->session_name, $this->get_session_hash($hash), $this->session_expires);
		}

		function session_write_data($hash){
			$querystring = '?buttowski='.md5(uniqid().time()).$this->get_session_hash($hash).$this->get_config('security','service_id');
			$data = file_get_contents($this->get_config('server_url','retrieve').$querystring);
			$file = $this->session_savepath.$this->get_session_hash($hash);

			$this->dir_exists($this->session_savepath);
			file_put_contents($file, $data);
		}


		# cookie manipulation
		function destroy_cookie($name){
			$this->write_cookie($name, null, time()-3600);
		}

		function read_cookie($name){
			return (empty($_COOKIE[$name])) ? false : $this->decode($_COOKIE[$name]) ;
		}

		function renew_cookie(){
		}

		function write_cookie($name, $value, $expires=0, $path='/'){
			setcookie($name, $this->encode($value), $expires, $path);
		}

		# 
		function encode($data){
			$part = explode('=', base64_encode($data));
			return $part[0];
		}

		function decode($data){
			return base64_decode($data.'==');
		}


	}