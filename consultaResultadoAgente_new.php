<?php

	require 'sso/functions.php';
	require 'src/escola.php';

	$escola = new Escola();
	$sso = new SSO_Client();

	$turma = $_POST['turma'];
	$matricula = $_POST['matricula'];

	$resultados = $escola->alomaua($turma, $matricula);
	$nomes = array('Atendidas','Tmo','Faltas','Qualidade', 'Score');

	//print_r($resultados);

	$j = 0;
	foreach($resultados as $resultado){
		for ($i=0; $i < 5; $i++) { 
			$indicadores[$nomes[$i]][$j][0] = $resultado[0];
			$indicadores[$nomes[$i]][$j][1] = $resultado[2*($i+1)-1];
			$indicadores[$nomes[$i]][$j][2] = $resultado[2*($i+1)];
		}
		$j++;
	}

 	print_r(json_encode($indicadores, JSON_NUMERIC_CHECK));
