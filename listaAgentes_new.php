<?php

	require 'sso/functions.php';
	require 'src/escola.php';

	$escola = new Escola();
	$sso = new SSO_Client();

	$sso->is_loggedin();

	echo "<label for=\"agentes\">Agente: </label>";
	echo $escola->combo_agentes($_POST['turma']);
