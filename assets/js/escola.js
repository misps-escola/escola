


function hideById(name){
	document.getElementById(name).style.display='none';
}

function showById(name){
	document.getElementById(name).style.display='block';
}

function objxml(){
	var objxml;
	if (window.XMLHttpRequest) {
		// code for IE7+, Firefox, Chrome, Opera, Safari
		var objxml =new XMLHttpRequest();
	}
	else {
		// code for IE6, IE5
		var objxml =new ActiveXObject("Microsoft.XMLHTTP");
	}
	return objxml;
}


function aloAjax(requisicao,destino,filtros){
	var xmlhttp = objxml()
	
	xmlhttp.onreadystatechange=function() {
	  if (xmlhttp.readyState==4 && xmlhttp.status==200) {
			document.getElementById(destino).innerHTML = xmlhttp.responseText;
		}
	}

	xmlhttp.open("POST", requisicao, false);
	xmlhttp.send(filtros);
}

function listaAgentes(){
	var turma = new FormData();
	var elemento = document.getElementById("seletor-turmas");
	var turmaSelecionada = elemento.options[elemento.selectedIndex].value;

	turma.append('turma',turmaSelecionada);
	
	aloAjax('./listaAgentes.php','agentes',turma);
}

function listaAgentes_new(){
	var turma = new FormData();
	var elemento = document.getElementById("seletor-turmas");
	var turmaSelecionada = elemento.options[elemento.selectedIndex].value;

	turma.append('turma',turmaSelecionada);
	aloAjax('./listaAgentes_new.php','agentes',turma);
}


function listaPeriodos(){
	var turma = new FormData();
	var elemento = document.getElementById("seletor-turmas");
	var turmaSelecionada = elemento.options[elemento.selectedIndex].value;

	turma.append('turma',turmaSelecionada);
	
	aloAjax('./listaPeriodos.php','periodos',turma);

	document.getElementById("resultados").innerHTML = '<h1>Selecione um periodo</h1>';
}

function listaPeriodos_new(){
	var turma = new FormData();
	var elemento = document.getElementById("seletor-turmas");
	var turmaSelecionada = elemento.options[elemento.selectedIndex].value;

	turma.append('turma',turmaSelecionada);
	aloAjax('./listaPeriodos_new.php','periodos',turma);
}

function tabelaTurma(){
	var filtro = new FormData();
	var seletorTurmas = document.getElementById("seletor-turmas");
	var turmaSelecionada = seletorTurmas.options[seletorTurmas.selectedIndex].value;
	var seletorPeriodo = document.getElementById("seletor-periodo");
	var periodoSelecionado = seletorPeriodo.options[seletorPeriodo.selectedIndex].value;

	filtro.append('turma',turmaSelecionada);
	filtro.append('periodo',periodoSelecionado);

	aloAjax('./tabelaAgentesTurma.php','resultados',filtro);

	$('#employees').dataTable({
		'sPaginationType': 'simple',
		'lengthMenu': [10, 20, 30],
		'language': {
		    'info': 'Página _PAGE_ de _PAGES_ (_TOTAL_ Resultados)',
		   	'infoEmpty': '',
		   	'infoFiltered': "",
		   	'lengthMenu': 'Exibir _MENU_ colaboradores',
		    'paginate': {
		    	'next': 'Próxima',
		    	'previous': 'Anterior'
		   	},
		   	'search': '',
		   	'zeroRecords': 'Não foram encontrados colaboradores com este critério de pesquisa.'
		}
	});
}

function tabelaTurma_new(){
	var filtro = new FormData();
	var seletorTurmas = document.getElementById("seletor-turmas");
	var turmaSelecionada = seletorTurmas.options[seletorTurmas.selectedIndex].value;

	filtro.append('turma',turmaSelecionada);

	aloAjax('./tabelaAgentesTurma_new.php', 'resultados', filtro);

	$('#employees').dataTable({
		'sPaginationType': 'simple',
		'lengthMenu': [20, 30],
		'language': {
		    'info': 'Página _PAGE_ de _PAGES_ (_TOTAL_ Resultados)',
		   	'infoEmpty': '',
		   	'infoFiltered': "",
		   	'lengthMenu': 'Exibir _MENU_ colaboradores',
		    'paginate': {
		    	'next': 'Próxima',
		    	'previous': 'Anterior'
		   	},
		   	'search': '',
		   	'zeroRecords': 'Não foram encontrados colaboradores com este critério de pesquisa.'
		}
	});
}

function drawChart(dados,destino,titulo) {
	 var data0 = google.visualization.arrayToDataTable([
	  ['Year', '0', '1'],
	  ['2004',  1000,      400],
	  ['2005',  0,      0],
	  ['2006',  0,       0],
	  ['2007',  1030,      540]
	]);

	var data1 = new google.visualization.DataTable;

	data1.addColumn('string', 'Periodo');
	data1.addColumn('number', 'Agente');
	data1.addColumn('number', 'Meta');

	data1.addRows(dados);

	var options = {
		title: titulo,
		animation:{
			duration: 1000,
			easing: 'out',
		},
		colors:['#00b8d4','#d41c00'],
		chartArea:{width:"70%"},
		titleTextStyle: { color: '#5c5c5c', fontName: 'Helvetica', fontSize: '12' }
	};

	var chart = new google.visualization.LineChart(document.getElementById(destino));
	chart.draw(data0, options);
	chart.draw(data1, options);


}

function drawChartC(dados,destino,titulo) {
	 var data0 = google.visualization.arrayToDataTable([
	  ['Year', '0', '1'],
	  ['2004', 1000, 400],
	  ['2005', 0,    0],
	  ['2006', 0,    0],
	  ['2007', 1030, 540]
	]);

	var data1 = new google.visualization.DataTable;

	data1.addColumn('string', 'Periodo');
	data1.addColumn('number', 'Agente');
	data1.addColumn('number', 'Meta');

	data1.addRows(dados);

	var options = {
		title: titulo,
		animation:{
			duration: 1000,
			easing: 'out',
		},
		colors:['#00b8d4','#d41c00'],
		chartArea:{width:"70%"},
		titleTextStyle: { color: '#5c5c5c', fontName: 'Helvetica', fontSize: '12' }
	};

	var chart = new google.visualization.ColumnChart(document.getElementById(destino));
	chart.draw(data0, options);
	chart.draw(data1, options);
}

function drawChartCC(dados,destino,titulo) {
	var data0 = google.visualization.arrayToDataTable([
	  ['Year', '0', '1'],
	  ['2004',  1000,      400],
	  ['2005',  0,      0],
	  ['2006',  0,       0],
	  ['2007',  1030,      540]
	]);

	var data1 = new google.visualization.DataTable;

	data1.addColumn('string', 'Periodo');
	data1.addColumn('number', 'Meta');
	data1.addColumn('number', 'Agente');

	data1.addRows(dados);

	var options = {
		title: titulo,
		animation:{
			duration: 1000,
			easing: 'out',
		},
		colors:['#434348', '#95CEFF'],
		chartArea: { width:"70%" },
		titleTextStyle: { color: '#5c5c5c', fontName: 'Helvetica', fontSize: '12' },
		seriesType: "bars",
	  	series: {0: {type: "line"}}
	};

	var chart = new google.visualization.ComboChart(document.getElementById(destino));

	chart.draw(data1, options);
}

function graficosAgenteTurma(){

	showById('resultados');

	var filtro = new FormData();
	var seletorTurmas = document.getElementById("seletor-turmas");
	var turmaSelecionada = seletorTurmas.options[seletorTurmas.selectedIndex].value;

	var agenteSelecionado = document.getElementById("seletor-agentes");
	var agenteSelecionado = agenteSelecionado.options[agenteSelecionado.selectedIndex].value;

	filtro.append('turma',turmaSelecionada);
	filtro.append('matricula',agenteSelecionado);
	
	aloAjax('./consultaResultadoAgente.php?agente='+agenteSelecionado+'&turma='+turmaSelecionada,'json',filtro);

	var json = JSON.parse(document.getElementById('json').innerHTML);
	drawChart(json.Atendidas,'atendidas','Atendidas');
	drawChart(json.Tmo,'tmo','TMO');
	drawChartC(json.Qualidade,'qualidade','Qualidade');
	drawChartC(json.Faltas,'faltas','Faltas');
}

function graficosAgenteTurma_new(){

	showById('resultados');

	var filtro = new FormData();
	var seletorTurmas = document.getElementById("seletor-turmas");
	var turmaSelecionada = seletorTurmas.options[seletorTurmas.selectedIndex].value;

	var agenteSelecionado = document.getElementById("seletor-agentes");
	var agenteSelecionado = agenteSelecionado.options[agenteSelecionado.selectedIndex].value;

	filtro.append('turma',turmaSelecionada);
	filtro.append('matricula',agenteSelecionado);
	
	aloAjax('./consultaResultadoAgente_new.php','json',filtro);

	var json = JSON.parse(document.getElementById('json').innerHTML);
	drawChartCC(json.Atendidas,'atendidas','Atendidas');
	drawChartCC(json.Tmo,'tmo','TMO');
	drawChartCC(json.Qualidade,'qualidade','Qualidade');
	drawChartCC(json.Faltas,'faltas','Faltas');
	drawChartCC(json.Score,'score','Score');
	
}

function mumicate(classe){
	if($(classe).css('display') === 'none'){
		$(classe+'-link').text('-');
		$(classe).show();

	}else{
		$(classe).hide();
		$(classe+'-link').text('+');

	}
}

