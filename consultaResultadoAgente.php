<?php
	require 'src/ConexaoBancoMisPg.php';
	require 'src/Agente.php';

	$turma = $_POST['turma'];
	$matricula = $_POST['matricula'];

	$agente = new Agente($turma, $matricula);
	$resultados = $agente->resultadosComparativo();

	$j = 0;

	$nomes = array('Atendidas','Tmo','Faltas','Qualidade');
	//$nomes = array('Atendidas','Tmo','Faltas','Qualidade', 'Score');

	foreach ($resultados as $resultado){
		for ($i=0; $i < 4; $i++) { 
			$indicadores[$nomes[$i]][$j][0] = $resultado[0];
			$indicadores[$nomes[$i]][$j][1] = $resultado[2*($i+1)-1];
			$indicadores[$nomes[$i]][$j][2] = $resultado[2*($i+1)];
		}
		$j++;
	}

 	print_r(json_encode($indicadores, JSON_NUMERIC_CHECK));
	