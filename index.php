<?php 

	require 'sso/functions.php';
	
	$sso = new SSO_Client();

	$sso->is_loggedin();

?>

<html>
	<head>
		<meta charset="utf-8">
		<title>Escola</title>
		<link rel="stylesheet" type="text/css" href="./assets/css/escola-dev.css" />
		<script type="text/javascript" src="./assets/js/escola.js"></script>
		<script type="text/javascript" src="./assets/js/jquery-2.0.1.min.js"></script>
		<script type="text/javascript" src="./assets/js/jquery.dataTables.js"></script>
	</head>
	<body>
		<div class="user-data">
			<div class="inner">
				<ul>
					<li class="fr logout"><a href="./sso/logout" class="btn-logout"><span class="fr descr-btn">Sair</span></a></li>
					<li class="fr user-meta"><span>Bem vindo(a), </span><span class="bold capitalized user-name"><?php echo $sso->get_user_data('full_name');?></span></li>
				</ul>
				<div class="clear"></div>
			</div>
		</div>

		<div class="banner">
			<div class="header">
				<div class="inner">
					<div class="fl header-logo">
						<a href="./" title="Ir para página inicial"><img src="./assets/img/logo.png" alt="" /></a>
					</div>
					<div class="fr header-menu">
						<ul>
							<li class="fr"><a href="./turmas">Resultados turma</a></li>
							<li class="fr"><a href="./agentes">Resultados agente</a></li>
						</ul>
					</div>
					<div class="fr header-breadcrumb" id="breadcrumb"></div>
					<div class="clear"></div>
				</div>
			</div>
			<div class="baseline"></div>
		</div>

		<div class="content ">
			<div class="inner">

				<div class="full-card">
					<div class="avatar fl">
						<img src="./assets/img/school.png" alt="">
					</div>
					<div class="description fr">
						<h2>Turmas</h2>
						<p>Relatório para acompanhamento da evolução das turmas. Aqui dividimos os períodos entre 30, 60, 90, 120, 180 dias posteriores ao inicio da turma na operação. Exibindo os indicadores de qtd. de ligações atendidas, tmo, qualidade e faltas.</p>
						<a href="turmas" >Acesse aqui</a>
					</div>
					<div class="clear"></div>
				</div><!--  -->

				<div class="full-card">
					<div class="avatar fr">
						<img src="./assets/img/agent.png" alt="">
					</div>
					<div class="description fl">
						<h2>Agentes</h2>
						<p>Todo histórico do agente disponível para consulta no período de acompanhamento (30, 60, 90, 120, 150 dias). Aqui comparamos os indicadores qtd. de atendidas, tmo, qualidade e faltas com os resultados da turma.</p>
						<a href="agentes" >Acesse aqui</a>
					</div>
					<div class="clear"></div>
				</div><!--  -->



				<div class="clear"></div>
			</div><!-- /end inner -->
		</div><!-- /end content -->

		<div class="footer">
			<div class="inner">
				<span>© <?php echo date('Y');?> Porto Seguro - Todos os direitos reservados.</span>
				<span class="fr"><a href="versao.mis">notas da versão 2.0.</a></span>
			</div>
		</div>

	
	</body>
</html>
