<?php 

	require './sso/functions.php';

	$sso = new SSO_Client();

	#echo (isset($_COOKIE['stark'])) ? 'usuário/senha' : 'error' ;

	if(isset($_COOKIE['stark'])):
		echo '<style>
	</style>';
	endif;
?>
<html>
	<head>
		<title>Login :: Escola de Atendimento</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">  
		<link href="./assets/css/loguinho.css" rel="stylesheet" type="text/css" />
		<!--<script type="text/javascript" src="./assets/js/jquery-2.0.1.min.js"></script>
		<script type="text/javascript" src="./assets/js/jquery.Modal.js"></script>-->
	</head>

	<body>
		<!-- LOGIN CONTAINER -->
		<div class="container-login">
			<div class="barra-usuario"></div>
			<div class="conteudo">
				<div class="caixa-login">
					<div class="cabecalho-caixa">
						<a href="./login.php"><img src="./assets/img/logo_full.png" /></a>
					</div>
					<div class="conteudo-caixa" >
						<div class="flutua-dir cc-direita">
							<form action="<?php echo $sso->get_config('server_url','login'); ?>" method="POST" >
								<input type="text" name="user" placeholder="Matricula" class="text" required autofocus /><br/>
								<input type="password" name="pass" placeholder="Senha" class="text" required /><br/>
								<input type="submit" value="ENTRAR" class="submit" />
								<input type="hidden" name="service" value="<?php echo $sso->get_config('security','service_id'); ?>" />
							</form>
						</div>
						<div class="flutua-dir cc-esquerda">
							<div class="badge">
								<img src="./assets/img/badge.png" id="cracha" />
							</div>
							<div class="login-fail">
								<img src="./assets/img/dados_incorretos.png" id="cracha" />
								<p id="mensagem">Usuário e/ou Senha incorretos</p>
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div><!-- /fim conteudo -->

			<div class="rodape">
			</div><!-- /fim footer -->
		</div>
		<!-- LOGIN CONTAINER -->
		<!-- MODAL -->
		<div class="modal" id="splash">
			
		</div>
		<!-- / END MODAL -->

		<?php if(empty($_COOKIE['splash'])){ ?>
		<script type="text/javascript">
			$(document).ready(function(){
				$('#splash').modal({
					escapeClose: false,
					clickClose: false,
					showClose: true,
					fadeDuration: 250,
					fadeDelay: 1.5
				});
			}); // ready function
		</script>
		<?php } ?>
	</body>
</html>