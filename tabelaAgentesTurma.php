<?php
	require 'src/ConexaoBancoMisPg.php';
	require 'src/Turma.php';

	$turma = $_POST['turma'];
	$data = $_POST['periodo'];

	$turma = new Turma($turma);

	#print_r($turma->resultadosMes($data));

	$json = $turma->resultadosMes($data);
?>
<html>
<head>
	<meta charset='utf-8'>
</head>
<body>

	<div class="userlist">
		<table class="table striped bordered hovered dataTable" id="employees">
			<thead>
				<tr>
					<th class="uppercase employee-name">nome</th>
					<th class="uppercase employee-indc">atendidas</th>
					<th class="uppercase employee-indc">tmo</th>
					<th class="uppercase employee-indc">faltas</th>
					<th class="uppercase employee-indc">qualidade</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($json as $row){ ?>
				<tr>
					<td class="capitalized "><?php echo $row[0];?></td>
					<td class="capitalized text-center"><?php echo $row[1];?></td>
					<td class="capitalized text-center"><?php echo $row[2];?></td>
					<td class="capitalized text-center"><?php echo $row[3];?></td>
					<td class="capitalized text-center"><?php echo $row[4];?></td>
				</tr>
				<?php }?>
			</tbody>
		</table>
	</div>
	<!-- /userlist -->
</body>
</htm