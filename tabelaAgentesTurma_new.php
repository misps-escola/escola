<?php

	require 'sso/functions.php';
	require 'src/escola.php';

	$escola = new Escola();
	$sso = new SSO_Client();

	$sso->is_loggedin();

	$turma = $_POST['turma'];
	#$tabela = $escola->resultados_tabela($turma, $periodo);

	$tabela = $escola->db->retornaArray("SELECT * FROM escola.proc_tabela_agentes_turmas({$turma})");

	if($tabela == false){
		echo "Não existem dados para a turma selecionada \"/";
	}
	else{

?>
<html>
<head>
	<meta charset='utf-8'>
</head>
<body>

	<div class="userlist scroll-h">
		<table class="table striped bordered hovered dataTable" id="employees">
			<thead>
				<tr>
					<th class="uppercase" style="min-width: 300px">nome</th>
					
					<th class="uppercase" style="width: 100px">
						<div class="descr">score (30)</div>
						<div class="expad"><a href="#" class="score-30-link" onclick="mumicate('.score-30');">+</a></div>
					</th>
					<th class="score-30 uppercase hide" colspan="3"><div class="descr">atd</div><div class="real-meta">(Real/Meta)</div></th>
					<th class="score-30 uppercase hide" colspan="3"><div class="descr">abs</div><div class="real-meta">(Real/Meta)</div></th>
					<th class="score-30 uppercase hide" colspan="3"><div class="descr">qld</div><div class="real-meta">(Real/Meta)</div></th>
					<th class="score-30 uppercase hide" colspan="3"><div class="descr">tmo</div><div class="real-meta">(Real/Meta)</div></th>

					<th class="uppercase" style="width: 100px">
						<div class="descr">score (60)</div>
						<div class="expad"><a href="#" class="score-60-link" onclick="mumicate('.score-60');">+</a></div>
					</th>
					<th class="score-60 uppercase hide" colspan="3"><div class="descr">atd</div><div class="real-meta">(Real/Meta)</div></th>
					<th class="score-60 uppercase hide" colspan="3"><div class="descr">abs</div><div class="real-meta">(Real/Meta)</div></th>
					<th class="score-60 uppercase hide" colspan="3"><div class="descr">qld</div><div class="real-meta">(Real/Meta)</div></th>
					<th class="score-60 uppercase hide" colspan="3"><div class="descr">tmo</div><div class="real-meta">(Real/Meta)</div></th>

					<th class="uppercase" style="width: 100px">
						<div class="descr">score (90)</div>
						<div class="expad"><a href="#" class="score-90-link" onclick="mumicate('.score-90');">+</a></div>
					</th>
					<th class="score-90 uppercase hide" colspan="3"><div class="descr">atd</div><div class="real-meta">(Real/Meta)</div></th>
					<th class="score-90 uppercase hide" colspan="3"><div class="descr">abs</div><div class="real-meta">(Real/Meta)</div></th>
					<th class="score-90 uppercase hide" colspan="3"><div class="descr">qld</div><div class="real-meta">(Real/Meta)</div></th>
					<th class="score-90 uppercase hide" colspan="3"><div class="descr">tmo</div><div class="real-meta">(Real/Meta)</div></th>

					<th class="uppercase" style="width: 100px">
						<div class="descr">score (120)</div>
						<div class="expad"><a href="#" class="score-120-link" onclick="mumicate('.score-120');">+</a></div>
					</th>
					<th class="score-120 uppercase hide" colspan="3"><div class="descr">atd</div><div class="real-meta">(Real/Meta)</div></th>
					<th class="score-120 uppercase hide" colspan="3"><div class="descr">abs</div><div class="real-meta">(Real/Meta)</div></th>
					<th class="score-120 uppercase hide" colspan="3"><div class="descr">qld</div><div class="real-meta">(Real/Meta)</div></th>
					<th class="score-120 uppercase hide" colspan="3"><div class="descr">tmo</div><div class="real-meta">(Real/Meta)</div></th>

					<th class="uppercase" style="width: 100px">
						<div class="descr">score (150)</div>
						<div class="expad"><a href="#" class="score-150-link" onclick="mumicate('.score-150');">+</a></div>
					</th>
					<th class="score-150 uppercase hide" colspan="3"><div class="descr">atd</div><div class="real-meta">(Real/Meta)</div></th>
					<th class="score-150 uppercase hide" colspan="3"><div class="descr">abs</div><div class="real-meta">(Real/Meta)</div></th>
					<th class="score-150 uppercase hide" colspan="3"><div class="descr">qld</div><div class="real-meta">(Real/Meta)</div></th>
					<th class="score-150 uppercase hide" colspan="3"><div class="descr">tmo</div><div class="real-meta">(Real/Meta)</div></th>

					<th class="uppercase" style="width: 100px">
						<div class="descr">score (180)</div>
						<div class="expad"><a href="#" class="score-180-link" onclick="mumicate('.score-180');">+</a></div>
					</th>
					<th class="score-180 uppercase hide" colspan="3"><div class="descr">atd</div><div class="real-meta">(Real/Meta)</div></th>
					<th class="score-180 uppercase hide" colspan="3"><div class="descr">abs</div><div class="real-meta">(Real/Meta)</div></th>
					<th class="score-180 uppercase hide" colspan="3"><div class="descr">qld</div><div class="real-meta">(Real/Meta)</div></th>
					<th class="score-180 uppercase hide" colspan="3"><div class="descr">tmo</div><div class="real-meta">(Real/Meta)</div></th>

				</tr>
			</thead>
			<tbody>


				<?php foreach($tabela as $row){ ?>
				<tr>
					<td class="capitalized"><?php echo $row[1];?></td>

					<!-- 30 -->
					<td class="text-center"><?php echo floor($row[14]);?></td>
					<td class="score-30 dados-indicador hide"><?php echo $row[2];?></td>
					<td class="score-30 dados-indicador hide"><?php echo $row[3];?></td>
					<td class="score-30 ico-quartil hide"><?php echo $escola->ico_quartil($row[4]);?></td>
					<td class="score-30 dados-indicador hide"><?php echo floor($row[8]);?></td>
					<td class="score-30 dados-indicador hide"><?php echo $row[9];?></td>
					<td class="score-30 ico-quartil hide"><?php echo $escola->ico_quartil($row[10]);?></td>
					<td class="score-30 dados-indicador hide"><?php echo $row[11];?></td>
					<td class="score-30 dados-indicador hide"><?php echo $row[12];?></td>
					<td class="score-30 ico-quartil hide"><?php echo $escola->ico_quartil($row[13]);?></td>
					<td class="score-30 dados-indicador hide"><?php echo floor($row[5]);?></td>
					<td class="score-30 dados-indicador hide"><?php echo $row[6];?></td>
					<td class="score-30 ico-quartil hide"><?php echo $escola->ico_quartil($row[7]);?></td>

					<!-- 60 -->
					<td class="text-center"><?php echo floor($row[27]);?></td>
					<td class="score-60 dados-indicador hide"><?php echo $row[15];?></td>
					<td class="score-60 dados-indicador hide"><?php echo $row[16];?></td>
					<td class="score-60 ico-quartil hide"><?php echo $escola->ico_quartil($row[17]);?></td>
					<td class="score-60 dados-indicador hide"><?php echo floor($row[21]);?></td>
					<td class="score-60 dados-indicador hide"><?php echo $row[22];?></td>
					<td class="score-60 ico-quartil hide"><?php echo $escola->ico_quartil($row[23]);?></td>
					<td class="score-60 dados-indicador hide"><?php echo $row[24];?></td>
					<td class="score-60 dados-indicador hide"><?php echo $row[25];?></td>
					<td class="score-60 ico-quartil hide"><?php echo $escola->ico_quartil($row[26]);?></td>
					<td class="score-60 dados-indicador hide"><?php echo floor($row[18]);?></td>
					<td class="score-60 dados-indicador hide"><?php echo $row[19];?></td>
					<td class="score-60 ico-quartil hide"><?php echo $escola->ico_quartil($row[20]);?></td>
					
					<!-- 90 -->
					<td class="text-center"><?php echo floor($row[40]);?></td>
					<td class="score-90 dados-indicador hide"><?php echo $row[28];?></td>
					<td class="score-90 dados-indicador hide"><?php echo $row[29];?></td>
					<td class="score-90 ico-quartil hide"><?php echo $escola->ico_quartil($row[30]);?></td>
					<td class="score-90 dados-indicador hide"><?php echo floor($row[34]);?></td>
					<td class="score-90 dados-indicador hide"><?php echo $row[35];?></td>
					<td class="score-90 ico-quartil hide"><?php echo $escola->ico_quartil($row[36]);?></td>
					<td class="score-90 dados-indicador hide"><?php echo $row[37];?></td>
					<td class="score-90 dados-indicador hide"><?php echo $row[38];?></td>
					<td class="score-90 ico-quartil hide"><?php echo $escola->ico_quartil($row[39]);?></td>
					<td class="score-90 dados-indicador hide"><?php echo floor($row[31]);?></td>
					<td class="score-90 dados-indicador hide"><?php echo $row[32];?></td>
					<td class="score-90 ico-quartil hide"><?php echo $escola->ico_quartil($row[33]);?></td>

					<!-- 120 -->
					<td class="text-center"><?php echo floor($row[53]);?></td>
					<td class="score-120 dados-indicador hide"><?php echo $row[41];?></td>
					<td class="score-120 dados-indicador hide"><?php echo $row[42];?></td>
					<td class="score-120 ico-quartil hide"><?php echo $escola->ico_quartil($row[43]);?></td>
					<td class="score-120 dados-indicador hide"><?php echo floor($row[47]);?></td>
					<td class="score-120 dados-indicador hide"><?php echo $row[48];?></td>
					<td class="score-120 ico-quartil hide"><?php echo $escola->ico_quartil($row[49]);?></td>
					<td class="score-120 dados-indicador hide"><?php echo $row[50];?></td>
					<td class="score-120 dados-indicador hide"><?php echo $row[51];?></td>
					<td class="score-120 ico-quartil hide"><?php echo $escola->ico_quartil($row[52]);?></td>
					<td class="score-120 dados-indicador hide"><?php echo floor($row[44]);?></td>
					<td class="score-120 dados-indicador hide"><?php echo $row[45];?></td>
					<td class="score-120 ico-quartil hide"><?php echo $escola->ico_quartil($row[46]);?></td>

					<!-- 150 -->
					<td class="text-center"><?php echo floor($row[66]);?></td>
					<td class="score-150 dados-indicador hide"><?php echo $row[54];?></td>
					<td class="score-150 dados-indicador hide"><?php echo $row[55];?></td>
					<td class="score-150 ico-quartil hide"><?php echo $escola->ico_quartil($row[56]);?></td>
					<td class="score-150 dados-indicador hide"><?php echo floor($row[60]);?></td>
					<td class="score-150 dados-indicador hide"><?php echo $row[61];?></td>
					<td class="score-150 ico-quartil hide"><?php echo $escola->ico_quartil($row[62]);?></td>
					<td class="score-150 dados-indicador hide"><?php echo $row[63];?></td>
					<td class="score-150 dados-indicador hide"><?php echo $row[64];?></td>
					<td class="score-150 ico-quartil hide"><?php echo $escola->ico_quartil($row[65]);?></td>
					<td class="score-150 dados-indicador hide"><?php echo floor($row[57]);?></td>
					<td class="score-150 dados-indicador hide"><?php echo $row[58];?></td>
					<td class="score-150 ico-quartil hide"><?php echo $escola->ico_quartil($row[59]);?></td>

					<!-- 180 -->
					<td class="text-center"><?php echo floor($row[79]);?></td>
					<td class="score-180 dados-indicador hide"><?php echo $row[67];?></td>
					<td class="score-180 dados-indicador hide"><?php echo $row[68];?></td>
					<td class="score-180 ico-quartil hide"><?php echo $escola->ico_quartil($row[69]);?></td>
					<td class="score-180 dados-indicador hide"><?php echo floor($row[73]);?></td>
					<td class="score-180 dados-indicador hide"><?php echo $row[74];?></td>
					<td class="score-180 ico-quartil hide"><?php echo $escola->ico_quartil($row[75]);?></td>
					<td class="score-180 dados-indicador hide"><?php echo $row[76];?></td>
					<td class="score-180 dados-indicador hide"><?php echo $row[77];?></td>
					<td class="score-180 ico-quartil hide"><?php echo $escola->ico_quartil($row[78]);?></td>
					<td class="score-180 dados-indicador hide"><?php echo floor($row[70]);?></td>
					<td class="score-180 dados-indicador hide"><?php echo $row[71];?></td>
					<td class="score-180 ico-quartil hide"><?php echo $escola->ico_quartil($row[72]);?></td>
				</tr>
				<?php } ?>
				



			</tbody>
		</table>
	</div>
	<!-- /userlist -->
</body>
</html>

<?php }?>