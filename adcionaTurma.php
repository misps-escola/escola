<?php 


	require './sso/functions.php';
	
	$sso = new SSO_Client();

	$sso->is_loggedin();

?>

<html>
	<head>
		<meta charset="utf-8">
		<title>Escola de atendimento - Turmas</title>
		<link rel="stylesheet" type="text/css" href="./assets/css/escola-dev.css" />
		<script type="text/javascript" src="./assets/js/escola.js"></script>
		<script type="text/javascript" src="./assets/js/jquery-2.0.1.min.js"></script>
		<script type="text/javascript" src="./assets/js/jquery.dataTables.js"></script>
	</head>
	<body>
		<div class="user-data">
			<div class="inner">
				<ul>
					<li class="fr logout"><a href="./sso/logout" class="btn-logout"><span class="fr descr-btn">Sair</span></a></li>
					<li class="fr user-meta"><span>Bem vindo(a), </span><span class="bold capitalized user-name"><?php echo $sso->get_user_data('full_name');?></span></li>
				</ul>
				<div class="clear"></div>
			</div>
		</div>

		<div class="banner">
			<div class="header">
				<div class="inner">
					<div class="fl header-logo">
						<a href="./"><img src="./assets/img/logo.png" alt="" /></a>
					</div>
					<div class="fr header-menu">
						<?php #$main->partial('header/menu'); ?>
					</div>
					<div class="fr header-breadcrumb" id="breadcrumb">
						<p><?php #echo $breadcrumb; ?>
							<ul>
								<li class="fr"><a href="./turmas.mis">Resultados turma</a></li>
								<li class="fr"><a href="./agentes.mis">Resultados agente</a></li>
							</ul>
						</p>
					</div>
					<div class="clear"></div>
				</div>
			</div>
			<div class="baseline"></div>
		</div>

		<div class="content ">
			<div class="inner">

				<div class="full-card">
				
				</div>
					
			<div class="clear"></div>

			</div><!-- /end inner -->
		</div><!-- /end content -->

		<div class="footer">
			<div class="inner">
				<span>© <?php echo date('Y');?> Porto Seguro - Todos os direitos reservados.</span>
			</div>
		</div>

	
	</body>
</html>
