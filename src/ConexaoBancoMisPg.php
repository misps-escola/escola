<?php
	error_reporting(0);
	
	class ConexaoBancoMisPg{

		function __construct(){
			$this->conexao = odbc_connect('MISPG','','');
		}

		function executaRetorno($strsql){
			$rs = odbc_exec($this->conexao,$strsql);

			if (odbc_error()){
               return 0;
         	}

			return $rs;
		}

		function retornaArray($strsql){
			
			$rs = $this -> executaRetorno($strsql);
			
			$campos = odbc_num_fields($rs);

			$i = 0;

			while($resultado = odbc_fetch_array($rs)){
		        for ($j = 1; $j <= $campos; $j++){
					$retorno[$i][$j-1] = $resultado[odbc_field_name($rs, $j)];
		        }
				$i++;
			}

			return $retorno;
		}

	}