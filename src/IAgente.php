<?php

	interface IAgente{

		public function resultadosComparativo();
		public function getTurma();
		public function getMatricula();
		public function getLogin();
		public function getServidor();
		public function getDac();
		
	}