<?php

	require 'IFiltroTurma.php';

	class FiltroTurma implements IFiltroTurma{

		private $turma;

		function __construct($turma){
			$this->turma = $turma;
		}

		private function consultaTurmas(){
			$con = new ConexaoBancoMisPg();
			
			$strsql = "SELECT * FROM agentes.proc_lista_turmas()";
		
			return $con -> retornaArray($strsql);
		}

		private function consultaAgentesTurma(){
			$con = new ConexaoBancoMisPg();

			$strsql = "SELECT * FROM agentes.proc_lista_agentes_turma(".$this -> turma.")";
		
			return $con->retornaArray($strsql);
		}

		private function consultaPeriodos(){
			$con = new ConexaoBancoMisPg();
			
			$strsql = "SELECT * FROM agentes.proc_lista_periodo(".$this -> turma.")";
		
			return $con->retornaArray($strsql);
		}

		public function listaTurmas($funcao){
			$turmas = $this->consultaTurmas();
			$html  = "<label for='turmas'>Turma: </label>";
			$html .= "<select onchange='". $funcao."' name='seletor-turmas' id='seletor-turmas' class ='seletor-turmas'>";
			$html .= "<option value=''>Selecione a turma</option>";

			while($turma = current($turmas)){
				$html .="<option value='".$turma[0]."'>".$turma[1]."</option>";
				next($turmas);
			}

			$html .= "</select>";
			return $html;
		}

		public function listaAgentesTurma(){
			$agentes = $this->consultaAgentesTurma();
			$html  = "<label for='agentes'>Agente: </label>";
			$html .= "<select name = 'seletor-agentes' onchange = 'graficosAgenteTurma();' id ='seletor-agentes' class ='seletor-agentes'>";
			$html .= "<option value=''>Selecione o agente</option>";
			
			while($agente = current($agentes)){
				$html .="<option value='".$agente[0]."'>".$agente[1]."</option>";
				next($agentes);
			}

			$html .= "</select>";
			return $html;
		}

		public function listaPeriodos(){
			$periodos = $this->consultaPeriodos();
			$html  = "<label for='periodo'>Periodo: </label>";
			$html .= "<select name = 'seletor-periodo' onchange = 'tabelaTurma();' id='seletor-periodo' class ='seletor-periodo'>";
			$html .= "<option value=''>Selecione o periodo</option>";

			while($periodo = current($periodos)){
				$html .="<option value='".$periodo[0]."'>".$periodo[1]."</option>";
				next($periodos);
			}

			$html .= "</select>";
			return $html;
		}

		public function getTurma(){
			return $this -> turma;
		}
	}