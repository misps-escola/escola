<?php 
		
	require 'ConexaoBancoMisPg.php';

	class Escola{

		function __construct(){
			$this->db = new ConexaoBancoMisPg();
		}

		# retrieve an property
		function get($option){
			return $this->$option;
		}		

		# set an property
		function set($option, $value){
			$this->$option = $value;
		}

		#
		function exibe_datahora($string){
			return date('d/m/Y H:i', strtotime($string));
		}

		#
		# $attributtes => array - Eg. array('class'=>'asdsadsa', 'onchange'=>'adsad', 'multiple'=>null);
		# return => string
		function element_attributes($attributes){
			$str_atttibutes = null;

			foreach($attributes as $attribute => $value){
				$str_atttibutes .= ($this->is_bool_attr($attribute)) ? "{$attribute} " : "{$attribute}=\"{$value}\" " ;
			}

			return $str_atttibutes;
		}

		# 
		# $attribute => string
		function is_bool_attr($attribute){
			$singles = array('checked', 'multiple', 'required');

			return in_array($attribute, $singles);
		}

		#
		# $name => string
		# $values => array - Eg. array(1=>'Opcao 1', 2=>'Opcao 2');
		# $attributtes => array - Eg. array('class'=>'asdsadsa', 'onchange'=>'adsad', 'multiple'=>null);
		# return => string
		function combo_builder($name, $options, $attributes=null){
			$html  = "<select name=\"{$name}\" {$this->element_attributes($attributes)}>";
			$html .= "	<option>Selecione</option>";
			$html .= "	<option>---</option>";

			foreach($options as $op_value => $op_name){
				$html .= "<option value=\"{$op_value}\">{$op_name}</option>";
			}

			$html .= "</select>";

			return $html;
		}


		# 
		# $view string - define o tipo de visao que será usada para gerar a consulta e os combos
		# return string
		function combo_turmas($view='agent'){
			$turmas = $this->resultados_turmas();
			$onchange = ($view == 'agent') ? 'listaAgentes_new();' : 'tabelaTurma_new();' ;
			$atributos = array(
				'class' => 'select seletor-turmas',
				'id' => 'seletor-turmas',
				'onchange' => $onchange);

			return $this->combo_builder('seletor-turmas', $turmas, $atributos);
		}

		# 
		# $view string - define o tipo de visao que será usada para gerar a consulta e os combos
		# return string
		function combo_agentes($turma){
			$agentes = $this->resultados_agentes($turma);
			$atributos = array(
				'class' => 'select seletor-agentes',
				'id' => 'seletor-agentes',
				'onchange' => 'graficosAgenteTurma_new();');

			return $this->combo_builder('seletor-agentes', $agentes, $atributos);
		}

		# 
		# $view string - define o tipo de visao que será usada para gerar a consulta e os combos
		# return string
		function combo_periodo($turma){
			$agentes = $this->resultados_periodo($turma);
			$atributos = array(
				'class' => 'select seletor-periodo',
				'id' => 'seletor-periodo',
				'onchange' => 'tabelaTurma_new();');

			return $this->combo_builder('seletor-periodo', $agentes, $atributos);
		}


		#
		# return array
		function resultados_turmas(){
			$resultSet = $this->db->executaRetorno("SELECT id, ds_turma, ds_operacao FROM escola.turma_operacao ORDER By ds_operacao");	

			$array = null;
			while($data = odbc_fetch_array($resultSet)) {
				$array[$data['id']] .= utf8_encode($data['ds_operacao'])." - turma {$data['ds_turma']}";
			}

			return $array;
		}

		#
		# return array
		function resultados_agentes($turma){
			$resultSet = $this->db->executaRetorno("SELECT DISTINCT id_matricula, nome_agente FROM escola.graficoweb_resultado WHERE id_turma_operacao={$turma} ORDER By nome_agente");

			$array = null;
			while($data = odbc_fetch_array($resultSet)) {
				$array[$data['id_matricula']] .= utf8_encode($data['nome_agente']);
			}

			return $array;
		}

		#
		# return array
		function resultados_periodo($turma){
			$resultSet = $this->db->executaRetorno("SELECT DISTINCT faixa_tempo as periodo FROM escola.graficoweb_resultado WHERE id_turma_operacao={$turma} ORDER By faixa_tempo");

			$array = null;
			while($data = odbc_fetch_array($resultSet)) {
				$array[$data['periodo']] .= "{$data['periodo']} dias";
			}

			return $array;
		}

		#		
		function alomaua($turma, $matricula){
			return $this->db->retornaArray("SELECT * FROM escola.proc_grafico_agentes({$turma}, {$matricula})");
		}

		function ico_quartil($quartil){
			return "<div class=\"quartil_{$quartil}\"></div>";
		}


	}


