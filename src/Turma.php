<?php
	
	require 'ITurma.php';

	class Turma implements ITurma{

		private $turma;

		function __construct($turma){
			$this->turma = $turma;
		}

		public function resultadosMes($mes){
			$con = new ConexaoBancoMisPg();
			
			$strsql = "SELECT * FROM agentes.proc_performance_escola_turma('".$mes."', ".$this -> turma .")";
		
			return $con -> retornaArray($strsql);
		}

		public function getTurma(){
			return $this -> turma;
		}
	}