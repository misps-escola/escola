<?php
	require 'IAgente.php';

	class Agente implements IAgente{

		private $turma;
		private $matricula;
		private $login;
		private $servidor;
		private $dac;

		function __construct($turma, $matricula){
			$this-> turma = $turma;
			$this-> matricula = $matricula;

			$con = new ConexaoBancoMisPg();
			$strsql = "SELECT * FROM agentes.proc_avaya_retorna_ultimo_login('".$this -> matricula ."')";
			$resultados = $con -> retornaArray($strsql);

			$this->login = $resultados['logid'];
			$this->servidor = $resultados['servidor'];
			$this->dac = $resultados['dac'];
		}
		
		public function resultadosComparativo(){
			$con = new ConexaoBancoMisPg();
			
			$strsql = "SELECT * FROM agentes.proc_performance_escola_agentes(".$this->turma.", '".$this->matricula ."')";

			return $con->retornaArray($strsql);
		}

		public function getTurma(){
			return $this->turma;
		}

		public function getMatricula(){
			return $this->matricula;
		}
	
		public function getLogin(){
			return $this->login;
		}

		public function getServidor(){
			return $this->servidor;
		}

		public function getDac(){
			return $this->dac;
		}


		function bazinga(){
		}
	}