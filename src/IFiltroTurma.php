<?php
	interface IFiltroTurma{

		public function listaTurmas($funcao);
		public function listaAgentesTurma();
		public function listaPeriodos();
		public function getTurma();

	}