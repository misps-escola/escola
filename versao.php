<?php 


	require './sso/functions.php';
	
	$sso = new SSO_Client();

	$sso->is_loggedin();

?>

<html>
	<head>
		<meta charset="utf-8">
		<title>Escola de atendimento - Turmas</title>
		<link rel="stylesheet" type="text/css" href="./assets/css/escola-dev.css" />
		<script type="text/javascript" src="./assets/js/escola.js"></script>
		<script type="text/javascript" src="./assets/js/jquery-2.0.1.min.js"></script>
		<script type="text/javascript" src="./assets/js/jquery.dataTables.js"></script>
	</head>
	<body>
		<div class="user-data">
			<div class="inner">
				<ul>
					<li class="fr logout"><a href="./sso/logout" class="btn-logout"><span class="fr descr-btn">Sair</span></a></li>
					<li class="fr user-meta"><span>Bem vindo(a), </span><span class="bold capitalized user-name"><?php echo $sso->get_user_data('full_name');?></span></li>
				</ul>
				<div class="clear"></div>
			</div>
		</div>

		<div class="banner">
			<div class="header">
				<div class="inner">
					<div class="fl header-logo">
						<a href="./" title="Ir para página inicial"><img src="./assets/img/logo.png" alt="" /></a>
					</div>
					<div class="fr header-menu">
						<?php #$main->partial('header/menu'); ?>
					</div>
					<div class="fr header-breadcrumb" id="breadcrumb">
						<p><?php #echo $breadcrumb; ?>
							<ul>
								<li class="fr"><a href="./turmas.mis">Resultados turma</a></li>
								<li class="fr"><a href="./agentes.mis">Resultados agente</a></li>
							</ul>
						</p>
					</div>
					<div class="clear"></div>
				</div>
			</div>
			<div class="baseline"></div>
		</div>

		<div class="content ">
			<div class="inner">

				<div class="version-notes">
					<div class="description">
						<h2>Versão 2.0</h2>
						<p>- Periodo de acompanhamento</br>
							&nbsp&nbsp&nbsp- 30 dias;</br>
							&nbsp&nbsp&nbsp- 60 dias;</br>
							&nbsp&nbsp&nbsp- 90 dias;</br>
							&nbsp&nbsp&nbsp- 120 dias;</br>
							&nbsp&nbsp&nbsp- 150 dias;</br>
							&nbsp&nbsp&nbsp- 180 dias.</br>

						</br>- Indicadores</br>
							&nbsp&nbsp&nbsp- Atendidas(Avaya);</br>
							&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp- Soma de atendidas no período.</br>
							&nbsp&nbsp&nbsp- TMO(Avaya);</br>
							&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp- Tempo médio em que o agente ficou em operação, isso é: tempo falado, em música e em pós-atendimento.</br>
							&nbsp&nbsp&nbsp- Faltas(Ronda);</br>
							&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp- São contabilizadas apenas os dias sem nenhum evento de trabalho.</br>
							&nbsp&nbsp&nbsp- Qualidade(SIQ).</br>
							&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp- Média ponderada das monitorias realizadas no período.</br>

						</br>- Filtros/Tags(Nomes nas tabelas)</br>
							&nbsp&nbsp&nbsp- Nome(Ronda);</br>
							&nbsp&nbsp&nbsp- Matricula(Ronda);</br>
							&nbsp&nbsp&nbsp- Login(CCM7);</br>
							&nbsp&nbsp&nbsp- Turma(Escola/Excel)</br>
							&nbsp&nbsp&nbsp- Período de acompanhamento(Escola/Excel)</br>

						</br>- Visões</br>
							&nbsp&nbsp&nbsp- Tabela</br>
							&nbsp&nbsp&nbsp- Gráficos </br>
								&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp- Com o comparativo Agente X Turma para todo o intervalo de acompanhamento do mesmo(30~180 dias).</br>
								&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp- Esses gráficos devem ser separados por indicador;</br>

						</br>- Atualização dos dados</br>
							&nbsp&nbsp&nbsp- Todas as informações serão atualizadas no prazo em que atualizamos elas para os outros relatórios, assim teremos:</br>
								&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp- Atendidas/TMO/Qualidade</br>
									&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspD - 1</br>
								&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp- Faltas </br>
									&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspD - 30</br>
					</p>
					</div>
					<div class="clear"></div>
				</div><!--  -->

				<div class="clear"></div>
			</div><!-- /end inner -->
		</div><!-- /end content -->

		<div class="footer">
			<div class="inner">
				<span>© <?php echo date('Y');?> Porto Seguro - Todos os direitos reservados.</span>
				<span class="fr"><a href="versao.mis">notas da versão 2.0.</a></span>
			</div>
		</div>

	
	</body>
</html>
